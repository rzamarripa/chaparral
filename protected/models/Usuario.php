<?php

/**
 * This is the model class for table "Usuario".
 *
 * The followings are the available columns in table 'Usuario':
 * @property string $id
 * @property string $nombre
 * @property string $contrasena
 * @property string $nombreUsuario
 * @property string $estatusid
 * @property string $tipoUsuarioid
 *
 * The followings are the available model relations:
 * @property Tweet[] $tweets
 * @property Estatus $estatus
 * @property TipoUsuario $tipoUsuario
 */
class Usuario extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('estatusid, tipoUsuarioid', 'required'),
			array('nombre, contrasena, nombreUsuario', 'length', 'max'=>100),
			array('estatusid', 'length', 'max'=>11),
			array('tipoUsuarioid', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombre, contrasena, nombreUsuario, estatusid, tipoUsuarioid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tweets' => array(self::HAS_MANY, 'Tweet', 'usuarioId'),
			'estatus' => array(self::BELONGS_TO, 'Estatus', 'estatusId'),
			'tipoUsuario' => array(self::BELONGS_TO, 'TipoUsuario', 'tipoUsuarioid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
			'contrasena' => 'Contrasena',
			'nombreUsuario' => 'Nombre Usuario',
			'estatusid' => 'Estatusid',
			'tipoUsuarioid' => 'Tipo Usuarioid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('contrasena',$this->contrasena,true);
		$criteria->compare('nombreUsuario',$this->nombreUsuario,true);
		$criteria->compare('estatusid',$this->estatusid,true);
		$criteria->compare('tipoUsuarioid',$this->tipoUsuarioid,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Usuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
