<?php

/**
 * This is the model class for table "Prospecto".
 *
 * The followings are the available columns in table 'Prospecto':
 * @property string $id
 * @property string $nombre
 * @property string $app
 * @property string $apm
 * @property string $direccion
 * @property string $telefono
 * @property string $correo
 * @property string $tipoServicioId
 * @property string $etapaVentaId
 * @property string $estatusId
 *
 * The followings are the available model relations:
 * @property EtapaVenta $etapaVenta
 * @property Estatus $id0
 * @property TipoServicio $tipoServicio
 */
class Prospecto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Prospecto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tipoServicioId, etapaVentaId, estatusId', 'required'),
			array('nombre, app, apm, telefono, correo', 'length', 'max'=>100),
			array('direccion', 'length', 'max'=>200),
			array('tipoServicioId, etapaVentaId, estatusId', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombre, app, apm, direccion, telefono, correo, tipoServicioId, etapaVentaId, estatusId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'etapaVenta' => array(self::BELONGS_TO, 'EtapaVenta', 'etapaVentaId'),
			'estatus' => array(self::BELONGS_TO, 'Estatus', 'estatusId'),
			'tipoServicio' => array(self::BELONGS_TO, 'TipoServicio', 'tipoServicioId'),
		); 
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
			'app' => 'Ap. Paterno',
			'apm' => 'Ap. Materno',
			'direccion' => 'Dirección',
			'telefono' => 'Teléfono',
			'correo' => 'Correo',
			'tipoServicioId' => 'Tipo de Servicio',
			'etapaVentaId' => 'Etapa de Venta',
			'estatusId' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('app',$this->app,true);
		$criteria->compare('apm',$this->apm,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('correo',$this->correo,true);
		$criteria->compare('tipoServicioId',$this->tipoServicioId,true);
		$criteria->compare('etapaVentaId',$this->etapaVentaId,true);
		$criteria->compare('estatusId',$this->estatusId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Prospecto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
