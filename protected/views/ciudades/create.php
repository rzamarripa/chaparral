<?php
/* @var $this CiudadesController */
/* @var $model Ciudades */

$this->breadcrumbs=array(
	'Ciudades'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Ciudades', 'url'=>array('index')),
	array('label'=>'Manage Ciudades', 'url'=>array('admin')),
);
?>

<h1>Create Ciudades</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>