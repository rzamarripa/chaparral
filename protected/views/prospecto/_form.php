<?php
/* @var $this ProspectoController */
/* @var $model Prospecto */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'prospecto-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'app'); ?>
		<?php echo $form->textField($model,'app',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'app'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'apm'); ?>
		<?php echo $form->textField($model,'apm',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'apm'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'direccion'); ?>
		<?php echo $form->textField($model,'direccion',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'direccion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefono'); ?>
		<?php echo $form->textField($model,'telefono',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'telefono'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'correo'); ?>
		<?php echo $form->textField($model,'correo',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'correo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipoServicioId'); ?>
		<?php echo $form->dropDownList($model,'tipoServicioId', CHtml::listData(TipoServicio::model()->findAll(), "id", "nombre"),array('empty'=>"Seleccione uno")); ?>
		<span style="color:red"><strong>
		<?php echo $form->error($model,'tipoServicioId'); ?>
		</strong></span>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'etapaVentaId'); ?>
		<?php echo $form->dropDownList($model,'etapaVentaId', CHtml::listData(EtapaVenta::model()->findAll(), "id", "nombre"),array('empty'=>"Seleccione uno")); ?>
		<?php echo $form->error($model,'etapaVentaId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estatusId'); ?>
		<?php echo $form->dropDownList($model,'estatusId', CHtml::listData(Estatus::model()->findAll(), "id", "nombre"),array('empty'=>"Seleccione uno")); ?>
		<?php echo $form->error($model,'estatusId'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->