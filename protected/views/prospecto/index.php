<?php
/* @var $this ProspectoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Prospectos',
);

$this->menu=array(
	array('label'=>'Create Prospecto', 'url'=>array('create')),
	array('label'=>'Manage Prospecto', 'url'=>array('admin')),
);
?>

<h1>Prospectos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
