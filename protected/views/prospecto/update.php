<?php
/* @var $this ProspectoController */
/* @var $model Prospecto */

$this->breadcrumbs=array(
	'Prospectos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Prospecto', 'url'=>array('index')),
	array('label'=>'Create Prospecto', 'url'=>array('create')),
	array('label'=>'View Prospecto', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Prospecto', 'url'=>array('admin')),
);
?>

<h1>Update Prospecto <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>