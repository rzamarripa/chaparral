<?php
/* @var $this ProspectoController */
/* @var $model Prospecto */

$this->breadcrumbs=array(
	'Prospectos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Prospecto', 'url'=>array('index')),
	array('label'=>'Create Prospecto', 'url'=>array('create')),
	array('label'=>'Update Prospecto', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Prospecto', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Prospecto', 'url'=>array('admin')),
);
?>

<h1>View Prospecto #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nombre',
		'app',
		'apm',
		'direccion',
		'telefono',
		'correo',
		array('name'=>'tipoServicioId',
		        'value'=>$model->tipoServicio->nombre,),
		array('name'=>'etapaVentaId',
					        'value'=>$model->etapaVenta->nombre,),
		array('name'=>'estatusId',
					        'value'=>$model->estatus->nombre,),	),
)); ?>
