<?php
/* @var $this ProspectoController */
/* @var $data Prospecto */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('app')); ?>:</b>
	<?php echo CHtml::encode($data->app); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apm')); ?>:</b>
	<?php echo CHtml::encode($data->apm); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion')); ?>:</b>
	<?php echo CHtml::encode($data->direccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono')); ?>:</b>
	<?php echo CHtml::encode($data->telefono); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('correo')); ?>:</b>
	<?php echo CHtml::encode($data->correo); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('tipoServicioId')); ?>:</b>
	<?php echo CHtml::encode($data->tipoServicioId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('etapaVentaId')); ?>:</b>
	<?php echo CHtml::encode($data->etapaVentaId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estatusId')); ?>:</b>
	<?php echo CHtml::encode($data->estatusId); ?>
	<br />

	*/ ?>

</div>