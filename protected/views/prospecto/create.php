<?php
/* @var $this ProspectoController */
/* @var $model Prospecto */

$this->breadcrumbs=array(
	'Prospectos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Prospecto', 'url'=>array('index')),
	array('label'=>'Manage Prospecto', 'url'=>array('admin')),
);
?>

<h1>Crear Prospecto</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>