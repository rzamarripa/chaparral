<?php
/* @var $this EtapaVentaController */
/* @var $model EtapaVenta */

$this->breadcrumbs=array(
	'Etapa Ventas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EtapaVenta', 'url'=>array('index')),
	array('label'=>'Manage EtapaVenta', 'url'=>array('admin')),
);
?>

<h1>Create EtapaVenta</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>