<?php
/* @var $this EtapaVentaController */
/* @var $model EtapaVenta */

$this->breadcrumbs=array(
	'Etapa Ventas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List EtapaVenta', 'url'=>array('index')),
	array('label'=>'Create EtapaVenta', 'url'=>array('create')),
	array('label'=>'Update EtapaVenta', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EtapaVenta', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EtapaVenta', 'url'=>array('admin')),
);
?>

<h1>View EtapaVenta #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nombre',
		'estatusId',
	),
)); ?>
