<?php
/* @var $this EtapaVentaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Etapa Ventas',
);

$this->menu=array(
	array('label'=>'Create EtapaVenta', 'url'=>array('create')),
	array('label'=>'Manage EtapaVenta', 'url'=>array('admin')),
);
?>

<h1>Etapa Ventas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
