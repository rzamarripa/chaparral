<?php
/* @var $this EtapaVentaController */
/* @var $model EtapaVenta */

$this->breadcrumbs=array(
	'Etapa Ventas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EtapaVenta', 'url'=>array('index')),
	array('label'=>'Create EtapaVenta', 'url'=>array('create')),
	array('label'=>'View EtapaVenta', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EtapaVenta', 'url'=>array('admin')),
);
?>

<h1>Update EtapaVenta <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>