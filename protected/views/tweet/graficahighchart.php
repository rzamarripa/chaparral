<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<?php
	$nombres = array();
	$valores = array();
	foreach($tweets as $tweet){
		$nombres[] = $tweet["usuario"];
		$valores[] = $tweet["cantidad"];
	}
	
echo "
<script type='text/javascript'>
$(function () {
        $('#graficaColumna').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Tweets por Usuario'
            },
            subtitle: {
                text: 'Fuente: Curso Yii Framework con HighCharts'
            },
            xAxis: {
                categories: [";
                	foreach($nombres as $nombre){
                		echo  "'" . $nombre . "',";
					}
			echo "
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Cantidad de Tweets'
                }
            },
            tooltip: {
                headerFormat: '<table>',
                pointFormat: '<tr><td>{series.name}: </td>' +
                    '<td><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Cantidad',
                data: [";
                foreach($valores as $valor){
                		echo  $valor . ",";
					}
			echo "]
    
            }]
        });
    });
</script>"; ?>

<div id="graficaColumna">
</div>