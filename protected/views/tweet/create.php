<?php
/* @var $this TweetController */
/* @var $model Tweet */

$this->breadcrumbs=array(
	'Tweets'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Tweet', 'url'=>array('index')),
	array('label'=>'Manage Tweet', 'url'=>array('admin')),
);
?>

<h1>Create Tweet</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>