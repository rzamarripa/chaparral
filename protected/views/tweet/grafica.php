<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<?php
    echo '<script type="text/javascript">
		// Load the Visualization API and the piechart package.
		google.load("visualization", "1.0", {"packages":["corechart"]});

		// Set a callback to run when the Google Visualization API is loaded.
		google.setOnLoadCallback(drawChart);

		// Callback that creates and populates a data table, 
		// instantiates the pie chart, passes in the data and
		// draws it.
		function drawChart() {

		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn("string", "Instituciones");
		data.addColumn("number", "Cantidad");
		data.addColumn({type: "string", role: "tooltip"});';
		foreach($tweets as $valor){
			echo 'data.addRows([["' . $valor['usuario'] . '",' . $valor['cantidad'] . ',"' . $valor['usuario'] . ':' . number_format($valor['cantidad']) . '"]]);';
		}

		echo '
		// Set chart options
		var options = {
			"title":"Tweets por Usuario",
			"width":400,
			"height":400,
			hAxis: {title: "Tweets"},
			vAxis: 
			{
				title: "Cant. de Tweets",
				viewWindow: 
				{					
					min: 0,
		        },	        
			},
			legend:"none",			
		};

		// Instantiate and draw our chart, passing in some options.
		var chart = new google.visualization.PieChart(document.getElementById("tweetsPastel"));
		chart.draw(data, options);
    }
    	
    </script>';
    
    echo '<script type="text/javascript">
		// Load the Visualization API and the piechart package.
		google.load("visualization", "1.0", {"packages":["corechart"]});

		// Set a callback to run when the Google Visualization API is loaded.
		google.setOnLoadCallback(drawChart);

		// Callback that creates and populates a data table, 
		// instantiates the pie chart, passes in the data and
		// draws it.
		function drawChart() {

		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn("string", "Instituciones");
		data.addColumn("number", "Cantidad");
		data.addColumn({type: "string", role: "tooltip"});';
		foreach($tweets as $valor){
			echo 'data.addRows([["' . $valor['usuario'] . '",' . $valor['cantidad'] . ',"' . $valor['usuario'] . ':' . number_format($valor['cantidad']) . '"]]);';
		}

		echo '
		// Set chart options
		var options = {
			"title":"Tweets por Usuario",
			"width":400,
			"height":400,
			hAxis: {title: "Tweets"},
			vAxis: 
			{
				title: "Cant. de Tweets",
				viewWindow: 
				{					
					min: 0,
		        },	        
			},
			legend:"none",
			colors:["#39AECF"],
		};

		// Instantiate and draw our chart, passing in some options.
		var chart = new google.visualization.ColumnChart(document.getElementById("tweetsColumnas"));
		chart.draw(data, options);
    }
    	
    </script>';
?>
<div class="row">
	<div class="span12">
		<div class="row">
			<div class="span6">
				<div id="tweetsPastel"></div>		
			</div>	
			<div class="span6">
				<div id="tweetsColumnas"></div>
			</div>
		</div>
	</div>
</div>