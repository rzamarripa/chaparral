# ************************************************************
# Sequel Pro SQL dump
# Versión 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.33)
# Base de datos: twitter
# Tiempo de Generación: 2013-12-27 23:56:31 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla Ciudades
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Ciudades`;

CREATE TABLE `Ciudades` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Ciudades` WRITE;
/*!40000 ALTER TABLE `Ciudades` DISABLE KEYS */;

INSERT INTO `Ciudades` (`id`, `nombre`)
VALUES
	(1,'Culiacán'),
	(2,'Mochis'),
	(3,'Mazatlán');

/*!40000 ALTER TABLE `Ciudades` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Estatus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Estatus`;

CREATE TABLE `Estatus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Estatus` WRITE;
/*!40000 ALTER TABLE `Estatus` DISABLE KEYS */;

INSERT INTO `Estatus` (`id`, `nombre`)
VALUES
	(1,'Activo'),
	(2,'Inactivo');

/*!40000 ALTER TABLE `Estatus` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla EtapaVenta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `EtapaVenta`;

CREATE TABLE `EtapaVenta` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL DEFAULT '',
  `estatusId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `etapaventa_estatus` (`estatusId`),
  CONSTRAINT `etapaventa_estatus` FOREIGN KEY (`estatusId`) REFERENCES `Estatus` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `EtapaVenta` WRITE;
/*!40000 ALTER TABLE `EtapaVenta` DISABLE KEYS */;

INSERT INTO `EtapaVenta` (`id`, `nombre`, `estatusId`)
VALUES
	(1,'Prospecto',1),
	(2,'Presentación',1),
	(3,'Negociación',1),
	(4,'Ganado',1),
	(5,'Perdido',1);

/*!40000 ALTER TABLE `EtapaVenta` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Personas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Personas`;

CREATE TABLE `Personas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `sexo` varchar(100) DEFAULT NULL,
  `fecha_f` date DEFAULT NULL,
  `ciudad_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `persona_ciudad` (`ciudad_id`),
  CONSTRAINT `persona_ciudad` FOREIGN KEY (`ciudad_id`) REFERENCES `ciudades` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla Prospecto
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Prospecto`;

CREATE TABLE `Prospecto` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL DEFAULT '',
  `app` varchar(100) NOT NULL DEFAULT '',
  `apm` varchar(100) NOT NULL DEFAULT '',
  `direccion` varchar(200) DEFAULT NULL,
  `telefono` varchar(100) DEFAULT NULL,
  `correo` varchar(100) NOT NULL DEFAULT '',
  `tipoServicioId` int(11) unsigned NOT NULL,
  `etapaVentaId` int(11) unsigned NOT NULL,
  `estatusId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `prospecto_tiposervicio` (`tipoServicioId`),
  KEY `prospecto_etapaventa` (`etapaVentaId`),
  KEY `prospecto_estatus` (`estatusId`),
  CONSTRAINT `prospecto_estatus` FOREIGN KEY (`estatusId`) REFERENCES `Estatus` (`id`),
  CONSTRAINT `prospecto_etapaventa` FOREIGN KEY (`etapaVentaId`) REFERENCES `EtapaVenta` (`id`),
  CONSTRAINT `prospecto_tiposervicio` FOREIGN KEY (`tipoServicioId`) REFERENCES `TipoServicio` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Prospecto` WRITE;
/*!40000 ALTER TABLE `Prospecto` DISABLE KEYS */;

INSERT INTO `Prospecto` (`id`, `nombre`, `app`, `apm`, `direccion`, `telefono`, `correo`, `tipoServicioId`, `etapaVentaId`, `estatusId`)
VALUES
	(10,'prueba','','','','','',2,3,1);

/*!40000 ALTER TABLE `Prospecto` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla TipoServicio
# ------------------------------------------------------------

DROP TABLE IF EXISTS `TipoServicio`;

CREATE TABLE `TipoServicio` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `estatusId` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tiposervicio_estatus` (`estatusId`),
  CONSTRAINT `tiposervicio_estatus` FOREIGN KEY (`estatusId`) REFERENCES `Estatus` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `TipoServicio` WRITE;
/*!40000 ALTER TABLE `TipoServicio` DISABLE KEYS */;

INSERT INTO `TipoServicio` (`id`, `nombre`, `estatusId`)
VALUES
	(1,'Básico',1),
	(2,'Medio',1),
	(3,'Completo',1);

/*!40000 ALTER TABLE `TipoServicio` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla TipoUsuario
# ------------------------------------------------------------

DROP TABLE IF EXISTS `TipoUsuario`;

CREATE TABLE `TipoUsuario` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL DEFAULT '',
  `estatusid` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tipousuario_estatus` (`estatusid`),
  CONSTRAINT `tipousuario_estatus` FOREIGN KEY (`estatusid`) REFERENCES `Estatus` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `TipoUsuario` WRITE;
/*!40000 ALTER TABLE `TipoUsuario` DISABLE KEYS */;

INSERT INTO `TipoUsuario` (`id`, `nombre`, `estatusid`)
VALUES
	(1,'Guest',1),
	(3,'Administrador',1),
	(4,'Bootstrap',1);

/*!40000 ALTER TABLE `TipoUsuario` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Tweet
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Tweet`;

CREATE TABLE `Tweet` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(140) NOT NULL DEFAULT '',
  `usuarioId` int(11) unsigned NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `tweet_usuario` (`usuarioId`),
  CONSTRAINT `tweet_usuario` FOREIGN KEY (`usuarioId`) REFERENCES `Usuario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Tweet` WRITE;
/*!40000 ALTER TABLE `Tweet` DISABLE KEYS */;

INSERT INTO `Tweet` (`id`, `descripcion`, `usuarioId`, `fechaCreacion`)
VALUES
	(1,'futbol = adios problemas AR7',6,'2013-12-11 16:15:23'),
	(2,'Mi primer tweet',2,'2013-12-09 17:46:00'),
	(3,'Mi segundo tweet',2,'2013-12-09 17:56:23'),
	(4,'Ahi va mi tweet',3,'2013-12-11 15:56:22'),
	(5,'wE..!!',4,'2013-12-11 16:03:52'),
	(6,'A que hr Salen x El Pan?? :P',4,'2013-12-11 16:04:58'),
	(7,'HOla soy monica',5,'2013-12-11 16:07:14'),
	(8,'eh we...ya no heches la camisa a la secadora...XD',5,'2013-12-11 16:08:23'),
	(9,'nomas te falto decir k Eres CaRmelo Edgar',4,'2013-12-11 16:08:53'),
	(10,'ya le dije a mi suegra we.. ke se me estan haciendo mas chicas.. :S :(',4,'2013-12-11 16:09:45'),
	(11,'No le hagan caso al sonris',7,'2013-12-11 16:10:00'),
	(12,'ok',5,'2013-12-11 16:10:12'),
	(13,'y las camisas tambien?',3,'2013-12-11 16:10:24'),
	(14,'arriba el america!! ',7,'2013-12-11 16:17:01'),
	(15,'Arriba el LEON... :D',4,'2013-12-16 15:32:04'),
	(16,'Gracias por los que participaron y me dieron a ganar 600 pesos :D :D :D}',4,'2013-12-16 15:33:06'),
	(17,'te dije weeeyy apuestale los otros 500',3,'2013-12-16 15:43:34'),
	(18,'aquí estará',2,'2013-12-16 16:09:59'),
	(19,'monica, como en la escuela... ni copiando le atinas jajaja',3,'2013-12-16 17:06:25'),
	(20,'ya me volvio a marka error... :/',5,'2013-12-23 15:18:51'),
	(21,'no copias bien las cosas we..!!',4,'2013-12-23 15:38:07'),
	(22,'probando probando',2,'2013-12-27 14:26:31'),
	(23,'123123',4,'2013-12-27 14:26:57'),
	(24,'probando probando',2,'2013-12-27 14:28:38'),
	(25,'probando 2',2,'2013-12-27 14:30:01'),
	(26,'este es otro ejemplo',2,'2013-12-27 14:37:29'),
	(27,'haber si es cierto',4,'2013-12-27 14:58:14'),
	(28,'haber si es cierto',4,'2013-12-27 15:09:06'),
	(29,'aqui va mi tweet',3,'2013-12-27 15:09:18'),
	(30,'HOla ejemplo de envio de correo',7,'2013-12-27 15:09:37'),
	(31,'es otro tweet',4,'2013-12-27 15:09:45'),
	(32,'again..!!',4,'2013-12-27 15:16:43'),
	(33,'123321',6,'2013-12-27 15:16:52'),
	(34,'Test de notificacion de tweet',5,'2013-12-27 15:18:00'),
	(35,'Follow the leader..!',4,'2013-12-27 15:18:08'),
	(36,'prueba final',2,'2013-12-27 15:20:26'),
	(37,'Ahora si.... aqui debe de caer el correo',3,'2013-12-27 15:21:32'),
	(38,'Roboot',4,'2013-12-27 15:22:58'),
	(39,'antonio rubio',6,'2013-12-27 15:23:31'),
	(40,'ANTONIO RUBIO',6,'2013-12-27 15:24:44'),
	(41,'A ver si ahora... fuck...',5,'2013-12-27 15:35:34'),
	(42,':(',5,'2013-12-27 15:37:04'),
	(43,':(',5,'2013-12-27 15:41:02'),
	(44,'probando notificaciónes flash',2,'2013-12-27 15:48:48'),
	(45,'va de nuevo',2,'2013-12-27 15:49:35'),
	(46,'aquí probando las notificaciones',2,'2013-12-27 15:52:38'),
	(47,'mayor duración',2,'2013-12-27 15:54:29'),
	(48,'goodbye twitter',4,'2013-12-27 16:00:21'),
	(49,'Checando mensajes flash',7,'2013-12-27 16:00:35'),
	(50,'lenovo',6,'2013-12-27 16:01:56'),
	(51,'ar7neveradapt',6,'2013-12-27 16:02:48'),
	(52,'avisos tipo flah',5,'2013-12-27 16:03:09'),
	(53,'curso yii,',4,'2013-12-27 16:04:00'),
	(54,'probando notificaciónes flash múltiples',2,'2013-12-27 16:04:02'),
	(55,'CreateTweet',6,'2013-12-27 16:07:13'),
	(56,'prueba imagen',2,'2013-12-27 16:10:04'),
	(57,'tipos de avisos flash o_0',5,'2013-12-27 16:10:52'),
	(58,'otra vez pork c me paso XD',5,'2013-12-27 16:12:20'),
	(59,'prueba multiples Flash!!',7,'2013-12-27 16:15:31'),
	(60,'chaparral',6,'2013-12-27 16:16:23'),
	(61,'probando flash messages',3,'2013-12-27 16:17:09'),
	(62,'probando flash messages',3,'2013-12-27 16:17:16'),
	(63,'Tweets',6,'2013-12-27 16:18:01');

/*!40000 ALTER TABLE `Tweet` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Usuario
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Usuario`;

CREATE TABLE `Usuario` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL DEFAULT '',
  `contrasena` varchar(100) NOT NULL DEFAULT '',
  `nombreUsuario` varchar(100) NOT NULL DEFAULT '',
  `correo` varchar(100) DEFAULT NULL,
  `estatusid` int(11) unsigned NOT NULL,
  `tipoUsuarioid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_estatus` (`estatusid`),
  KEY `usuario_tipousuario` (`tipoUsuarioid`),
  CONSTRAINT `usuario_estatus` FOREIGN KEY (`estatusid`) REFERENCES `Estatus` (`id`),
  CONSTRAINT `usuario_tipousuario` FOREIGN KEY (`tipoUsuarioid`) REFERENCES `TipoUsuario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Usuario` WRITE;
/*!40000 ALTER TABLE `Usuario` DISABLE KEYS */;

INSERT INTO `Usuario` (`id`, `nombre`, `contrasena`, `nombreUsuario`, `correo`, `estatusid`, `tipoUsuarioid`)
VALUES
	(1,'Guest','123','Guest',NULL,1,1),
	(2,'Roberto','123','zama','rzamarripa@freenternet.com',1,3),
	(3,'Alejandro','123','alex','alex@agricolachaparral.com',1,3),
	(4,'Orduño','123','orduno','a.orduno@agricolachaparral.com',1,3),
	(5,'Edgar','123','edgar','ecalderon@agricolachaparral.com',1,3),
	(6,'Antonio','123','toño','antonio_ronaldo7@live.com',1,3),
	(7,'Monica','123','monica','monicacruz@agricolachaparral.com',1,3),
	(9,'Edgar Gregorio','123','edgarboot',NULL,1,4),
	(10,'Monica Cruz','123','esquer',NULL,1,4),
	(11,'Alexandro Stringel','123','astringel',NULL,1,4),
	(12,'Roberto Zamarripa','123','zamaboot',NULL,1,4),
	(13,'aorduno','123|','aorduno',NULL,1,4),
	(14,'Antonio Rubio','123','ar7neveradapt',NULL,1,4);

/*!40000 ALTER TABLE `Usuario` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
