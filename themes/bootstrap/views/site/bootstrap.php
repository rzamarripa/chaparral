<?php 
//Todo esto
$c = 0;?>

<div class="tabbable"> <!-- Only required for left/right tabs -->
  <ul class="nav nav-tabs">
  	<?php foreach($tipoServicios as $tipoServicio){ $c++;?>
		<li class="<?php echo ($c == 1) ? 'active' : ''; ?>">
			<a href="#tab<?php echo $c; ?>" data-toggle="tab"><?php echo $tipoServicio->nombre; ?></a>
		</li>
  	<?php } $c= 0;?>
  </ul>
  <div class="tab-content">
	<div class="tab-pane active" id="tab1">			
		<h1>Básico</h1>
		<table class="table table-striped table-bordered table-condensed">
			<thead class="thead">
				<tr>
					<th>Acciones</th>
					<th>Nombre</th>
					<th>Etapa de Ventas</th>
					<th>Tipo Servicio</th>
					<th>Estatus</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($prospectos as $prospecto){ 
						if($prospecto->tipoServicioId == 1){
							$claseEstatus = ($prospecto->estatusId == 1) ? "success" : "important";
				?>
				<tr>
					<td>
						<div class="btn-toolbar">
						    <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
						        'type'=>'warning', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'buttons'=>array(
        array('label'=>'Seleccionar', 'url'=>Yii::app()->createUrl('prospecto/view', array('id' => $prospecto->id)), 'icon' =>'ok white'),
        array('items'=>array(
            array('label'=>'Actualizar', 'url'=>Yii::app()->createUrl('prospecto/update', array('id' => $prospecto->id)), 'icon' =>'pencil'),
            array('label'=>($prospecto->estatusId == 1) ? 'Deshabilitar' : 'Habilitar', 'url'=>Yii::app()->createUrl('prospecto/cambiarestatus', array('id' => $prospecto->id,'estatus' => ($prospecto->estatusId == 1) ? 2 : 1)), 'icon' =>'refresh'),
            
            '---',
            array('label'=>'Eliminar', 'url'=>Yii::app()->createUrl('prospecto/delete', array('id' => $prospecto->id)), 'icon' =>'trash'),
        )),
    ),
						    )); ?>
						</div>
					</td>
					<td><?php echo $prospecto->nombre;?></td>	
					<td><?php echo $prospecto->etapaVenta->nombre;?></td>			
					<td><?php echo $prospecto->tipoServicio->nombre;?></td>			
					<td><span class="label label-<?php echo $claseEstatus; ?>"><?php echo $prospecto->estatus->nombre;?></span></td>			
				</tr>
				<?php } 
					}
				?>
			</tbody>
		</table>
		<div style="height:100px;"></div>
    </div>
    <div class="tab-pane" id="tab2">			
		<h1>Medio</h1>
		<table class="table table-striped table-bordered table-condensed">
			<caption><h4>Listado de participantes</h4></caption>
			<thead class="thead">
				<tr>
					<th>Acciones</th>
					<th>Nombre</th>
					<th>Etapa de Ventas</th>
					<th>Tipo Servicio</th>
					<th>Estatus</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($prospectos as $prospecto){ 
						if($prospecto->tipoServicioId == 2){
							$claseEstatus = ($prospecto->estatusId == 1) ? "success" : "important";
				?>
				<tr>
					<td>
						<div class="btn-toolbar">
						    <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
						        'type'=>'info', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'buttons'=>array(
        array('label'=>'Seleccionar', 'url'=>Yii::app()->createUrl('prospecto/view', array('id' => $prospecto->id)), 'icon' =>'ok white'),
        array('items'=>array(
            array('label'=>'Actualizar', 'url'=>Yii::app()->createUrl('prospecto/update', array('id' => $prospecto->id)), 'icon' =>'pencil'),
            array('label'=>($prospecto->estatusId == 1) ? 'Deshabilitar' : 'Habilitar', 'url'=>Yii::app()->createUrl('prospecto/cambiarestatus', array('id' => $prospecto->id,'estatus' => ($prospecto->estatusId == 1) ? 2 : 1)), 'icon' =>'refresh'),
            
            '---',
            array('label'=>'Eliminar', 'url'=>Yii::app()->createUrl('prospecto/delete', array('id' => $prospecto->id)), 'icon' =>'trash'),
        )),
    ),
						    )); ?>
						</div>
					</td>
					<td><?php echo $prospecto->nombre;?></td>	
					<td><?php echo $prospecto->etapaVenta->nombre;?></td>			
					<td><?php echo $prospecto->tipoServicio->nombre;?></td>			
					<td><span class="label label-<?php echo $claseEstatus; ?>"><?php echo $prospecto->estatus->nombre;?></span></td>			
				</tr>
				<?php } 
					}
				?>
			</tbody>
		</table>
		<div style="height:100px;"></div>
    </div>
    <div class="tab-pane" id="tab3">			
		<h1>Completo</h1>
		<table class="table table-striped table-bordered table-condensed">
			<caption><h4>Listado de participantes</h4></caption>
			<thead class="thead">
				<tr>
					<th>Acciones</th>
					<th>Nombre</th>
					<th>Etapa de Ventas</th>
					<th>Tipo Servicio</th>
					<th>Estatus</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($prospectos as $prospecto){ 
						if($prospecto->tipoServicioId == 3){
							$claseEstatus = ($prospecto->estatusId == 1) ? "success" : "important";
				?>
				<tr>
					<td>
						<div class="btn-toolbar">
						    <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
						        'type'=>'warning', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'buttons'=>array(
        array('label'=>'Seleccionar', 'url'=>Yii::app()->createUrl('prospecto/view', array('id' => $prospecto->id)), 'icon' =>'ok white'),
        array('items'=>array(
            array('label'=>'Actualizar', 'url'=>Yii::app()->createUrl('prospecto/update', array('id' => $prospecto->id)), 'icon' =>'pencil'),
            array('label'=>($prospecto->estatusId == 1) ? 'Deshabilitar' : 'Habilitar', 'url'=>Yii::app()->createUrl('prospecto/cambiarestatus', array('id' => $prospecto->id,'estatus' => ($prospecto->estatusId == 1) ? 2 : 1)), 'icon' =>'refresh'),
            
            '---',
            array('label'=>'Eliminar', 'url'=>Yii::app()->createUrl('prospecto/delete', array('id' => $prospecto->id)), 'icon' =>'trash'),
        )),
    ),
						    )); ?>
						</div>
					</td>
					<td><?php echo $prospecto->nombre;?></td>	
					<td><?php echo $prospecto->etapaVenta->nombre;?></td>			
					<td><?php echo $prospecto->tipoServicio->nombre;?></td>			
					<td><span class="label label-<?php echo $claseEstatus; ?>"><?php echo $prospecto->estatus->nombre;?></span></td>			
				</tr>
				<?php } 
					}
				?>
			</tbody>
		</table>
		<div style="height:100px;"></div>
    </div>
  </div>
</div>
