<?php /*
<table class= "table table-striped table-condensed table-bordered table-hover">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Usuario</th>
			<th>Descripcion</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($tweets as $tweet){ ?>
		<tr>
			<td><?php echo $tweet->usuario->nombre; ?></td>
			<td><?php echo $tweet->usuario->nombreUsuario; ?></td>
			<td><?php echo $tweet->descripcion; ?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>
*/ ?>

<div class="row">
	<div class="span6 offset4">
<?php 
	foreach($tweets as $tweet){ ?>
	<div class="row well well-small">
		<div class="span1">
			<img style="width:70px; height:70px;" class="img-rounded" 
				alt="foto"/> 
		</div>
		<div class="span4">					
			<span>
				<strong><?php echo $tweet->usuario->nombre; ?></strong>
				<?php echo '  @'.$tweet->usuario->nombreUsuario; ?>								
				<span class="pull-right label label-info"><?php echo date('d-m-Y H:i:s', strtotime($tweet->fechaCreacion)); ?></span>
			</span>
			<p><?php echo $tweet->descripcion; ?></p><br/>
			<?php if($tweet->usuarioId != $usuarioActual->id){ ?>
					<i class="icon icon-retweet"></i>
					<?php echo CHtml::link('Retweet',array('retweets/retweetear', 'usuario'=>$usuarioActual->id, 'tweet'=>$tweet->id)); 
				} else { ?>
					<i class="icon icon-trash"></i>
					<?php echo CHtml::link('Eliminar',array('retweets/delete', 'id'=>$tweet->id)); 
				}
            ?>
		</div>
	</div>			
<?php } ?>
	</div>
</div>