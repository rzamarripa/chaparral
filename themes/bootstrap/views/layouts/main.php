<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php Yii::app()->bootstrap->register(); ?>
</head>

<body>
<?php 
$items = array();

if(!Yii::app()->user->isGuest){
	$usuarioActual = Usuario::model()->find('nombreUsuario=:x',array(':x'=>Yii::app()->user->name));

	if($usuarioActual->tipoUsuario->nombre == "Administrador"){
		$items=array(
		        array(
		            'class'=>'bootstrap.widgets.TbMenu',
		            'htmlOptions'=>array('class'=>'pull-left'),
		            'items'=>array(
		                array('label'=>'Inicio', 'url'=>array('site/index'),'icon'=>'home'),	                
			            array('label'=>'Catálogos', 'url'=>array('#'), 'icon'=>'file', 'items'=>array(		                
			                array('label'=>'Estatus', 'url'=>array('estatus/index')),
			                array('label'=>'Tweets', 'url'=>array('tweet/index')),
			                array('label'=>'Tipo Usuarios', 'url'=>array('tipoUsuario/index')),
			                array('label'=>'Usuarios', 'url'=>array('usuario/index')),
			                array('label'=>'Personas', 'url'=>array('personas/index')),		                
							),
						),
						array('label'=>'Gráficas', 'url'=>array('#'), 'icon'=>'file', 'items'=>array(		                
							array('label'=>'Gráfica Google', 'url'=>array('tweet/grafica'),'icon'=>'signal'),
							array('label'=>'Gráfica Highcharts', 'url'=>array('tweet/graficahighchart'),'icon'=>'signal'),
							),
						),
						
		            ),
		        ),
		     );
	}
}
else{
	$usuarioActual = Usuario::model()->find("nombreUsuario = 'Guest'");
}

$items[] = array(
	  'class'=>'bootstrap.widgets.TbMenu',
	  'htmlOptions'=>array('class'=>'pull-right'),
	  'encodeLabel'=>false,
	  'items'=>array(  
	  	array('label'=>$usuarioActual->nombreUsuario, 'url'=>array('/perfil/view'), 'visible'=>!Yii::app()->user->isGuest, 				'htmlOptions'=>array('class'=>'btn'), 'icon'=>'user','items'=>array(		                
					array('label'=>'Cambiar contraseña', 'icon'=>'wrench','url'=> array('usuario/cambiar', 'id'=>$usuarioActual->id)),
		            )),
	  	array('label'=>'Iniciar Sesión', 'icon'=>'off', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest, 'htmlOptions'=>array('class'=>'btn')),
	    array('label'=>'Cerrar Sesión', 'icon'=>'off', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest, 'htmlOptions'=>array('class'=>'btn'))
	  ),
	);


$this->widget('bootstrap.widgets.TbNavbar',array(
    'items'=>$items,
)); 


		
?>

<div class="container" id="page">

	<?php 
	$flashMessages = Yii::app()->user->getFlashes();
		if($flashMessages){
			foreach($flashMessages as $key => $message){
				echo '<div class="info alert alert-'.$key.'" style="text-align:center">
							<button type="button" class="close" data-dismiss="alert">&times;</button>';
				echo '<p>' . $message . '</p>';
				echo '</div>';
			}
		}
	
	if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer" class="span4 offset4">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>

<?php
	Yii::app()->clientScript->registerScript(
		'myHideEffect',
		'$(".info").animate({opacity:.6},5000).slideUp("slow");',
		CClientScript::POS_READY
	);
?>